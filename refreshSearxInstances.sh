#!/bin/bash
#####################################################################
#
# Query top searx instances from searx.space with given
# filters and update html file
#
# Date:         2022-08-01
# Version:      0.4
#
# Changelog:    0.3 - Added a argument in the jq for the version
#               0.4 - Query for the newest version integrated
#
#####################################################################

logFile="/var/log/refreshSearxInstances.log"
instancesFile="instances.txt"
htmlFile="/usr/share/nginx/html/blog.maurix.net/bl-content/pages/searx/index.txt"
wantedVersion=`curl -L https://docs.searxng.org/ | grep '<title>' | cut -d "(" -f2 | cut -d ")" -f1`

if [ -e $logFile ]; then

    LOGFILESIZE=$(wc -c <"$logFile")
    if [ $LOGFILESIZE -gt 5000000 ]; then
        mv $logFile "$logFile.$(date +%Y%m%d)"
        gzip "$logFile.$(date +%Y%m%d)"
    fi
fi

echo "***********************************" >> $logFile
echo `date` " - Starting..." >> $logFile

if [ -e $instancesFile ]; then
    rm -f $instancesFile
fi

curl -s https://searx.space/data/instances.json | jq --arg v "$wantedVersion" '.instances | to_entries[] | select(any(.value; .version != null)) | select(any(.value; .version | startswith($v))) | select(any(.value; .http.grade=="A+")) | select(any(.value; .tls.grade=="A+")) | select(any(.value; .timing.search_go.success_percentage==100)) | select(any(.value; .timing.search.all.median<1.1)).key' > $instancesFile

if [ $? -eq 0 ]; then

    echo "Fetched instances:" >> $logFile
    cat $instancesFile >> $logFile
else
    echo "Fetching instances.json from https://searx.space/ failed!" >> $logFile
    echo `date` " - ERROR." >> $logFile
    exit 1
fi

sed -i 's/"/<br>/g' $instancesFile
sed -i '1,/INSTANCES-->/!d' $htmlFile
sed -i "/INSTANCES-->/ r $instancesFile" $htmlFile

oldDate=`egrep '[[:digit:]]{1,2}\.[[:digit:]]{1,2}\.[[:digit:]]{1,4}' $htmlFile | sed 's/<\/\?[^>]\+>//g' | tr -d '[:cntrl:]'`
newDate=`date +%d.%m.%Y | tr -d '[:cntrl:]'`

echo "Old date: ${oldDate}" >> $logFile
echo "New date: ${newDate}" >> $logFile

sed -i "s/$oldDate/$newDate/g" $htmlFile

echo `date` " - SUCCESS." >> $logFile